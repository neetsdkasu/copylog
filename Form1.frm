VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "コピー記録(0/0)"
   ClientHeight    =   2370
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   4320
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   4320
   StartUpPosition =   2  '画面の中央
   WindowState     =   1  '最小化
   Begin VB.CommandButton Command1 
      Caption         =   "Copy All"
      Default         =   -1  'True
      Height          =   495
      Left            =   3600
      TabIndex        =   2
      Top             =   1800
      Width           =   615
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   1695
      Left            =   3600
      Max             =   0
      TabIndex        =   1
      Top             =   0
      Width           =   615
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   120
      Top             =   2280
   End
   Begin VB.TextBox Text1 
      Height          =   2250
      Left            =   135
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  '両方
      TabIndex        =   0
      Text            =   "Form1.frx":0000
      Top             =   0
      Width           =   3345
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "メニュー(&M)"
      Begin VB.Menu mnuReset 
         Caption         =   "リセット(&R)"
      End
      Begin VB.Menu mnuBar 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "終了(&X)"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Str1() As String
Dim StrLast As String

Private Sub Command1_Click()
    Dim s As String
    
    s = Text1.Text
    
    If s = "" Then
        Exit Sub
    End If
    
    Clipboard.Clear
    Clipboard.SetText s
    StrLast = s
    Text1.SelStart = 0
    Text1.SelLength = Len(s) - 1
    
End Sub

Private Sub Form_Load()

    ReDim Str1(0)
    
    Text1.Text = ""
    
    If Clipboard.GetFormat(vbCFText) Then
        StrLast = Clipboard.GetText()
    End If
    
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuReset_Click()
    ReDim Str1(0)
    VScroll1.Value = 0
    VScroll1.Max = 0
    Text1.Text = ""
End Sub

Private Sub Timer1_Timer()
    Dim n As Integer
    Dim s As String
    
    n = UBound(Str1)
    
    If Clipboard.GetFormat(vbCFText) Then
        
        s = Clipboard.GetText()
        
        If s <> StrLast Then
            
            Str1(n) = s
            StrLast = s
            
            Text1.Text = s
            VScroll1.Max = n
            VScroll1.Value = n
            
            n = n + 1
            
            Me.Caption = "コピー記録(" & n & "/" & n & ")"
            
            ReDim Preserve Str1(n)
                        
        End If
        
    End If
    
End Sub

Private Sub VScroll1_Change()
    Dim n As Integer
    Dim m As Integer
    
    n = VScroll1.Value
    m = VScroll1.Max
    
    Text1.Text = Str1(n)
    
    Me.Caption = "コピー記録(" & (n + 1) & "/" & (m + 1) & ")"

End Sub
